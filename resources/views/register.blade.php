<!DOCTYPE html>
<html>
    <form action="/welcome" method="POST">
        <head>
            <title>Form SignUp</title>
        </head>

        @csrf
        
        <body>
            <h1>Buat Akun Baru!</h1>
            <h2>SignUp Form</h2>
            <p>
                <label>First Name:</label></br>
                <input type="text" name="firstname" placeholder="Nama Depan" />
            </p>
            <p>
                <label>Last Name:</label></br>
                <input type="text" name="lastname" placeholder="Nama Belakang">
            </p>
            <p>
                <label>Gender:</label></br>
                    <input type="radio" name="gender" value="male" />Male</br>
                    <input type="radio" name="gender" value="female" />Female</br>
                    <input type="radio" name="gender" value="other" />Other</br>
            </p>
            <p>
                <label>Nationality:</label></br>
                <select name="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="japan">Japan</option>
                    <option value="autralia">Australia</option>
                    <option value="america">America</option>
                </select>
            </p>
            <p>
                <label>Language Spoken:</label></br>
                <input type="checkbox" name="bahasa" value="bahasa" /> Bahasa Indonesia</br>
                <input type="checkbox" name="english" value="english" /> English</br>
                <input type="checkbox" name="other" value="other" /> Other</br>
                    <input type="text" name="otherlang" /> 
            </p>
            <p>
                <label>Bio:</label></br>
                <textarea name="bio"></textarea>
            </p>
            <p>
                <input type="submit" name="signup" value="SignUp"/>
            </p>
    
        </body>
    </form> 
</html>